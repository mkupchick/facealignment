import cv2
import numpy as np
# TODO: add target feature size

def hogFromLandmarks(img, landmarks):
    locations = [tuple(x) for x in landmarks]
    winSize = (32, 32)
    blockSize = (16, 16)
    blockStride = (8, 8)
    cellSize = (8, 8)
    nbins = 9
    derivAperture = 1
    winSigma = 4.
    histogramNormType = 0
    L2HysThreshold = 2.0000000000000001e-01
    gammaCorrection = 0
    nlevels = 64
    hog = cv2.HOGDescriptor(winSize, blockSize, blockStride, cellSize, nbins, derivAperture, winSigma,
                            histogramNormType, L2HysThreshold, gammaCorrection, nlevels)
    winStride = (8, 8)
    padding = (8, 8)
    return hog.compute(img, winStride, padding, locations)

def siftFromLandmarks(img, landmarks):
    gray = cv2.cvtColor(img, cv2.COLOR_RGBA2GRAY)
    kp = [cv2.KeyPoint(x[0], x[1], 1) for x in landmarks]
    sift = cv2.xfeatures2d.SIFT_create()
    kp, features = sift.compute(gray, kp)
    return features / 255.0
