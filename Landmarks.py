import numpy as np
import os
import cv2

def readGTLandmarks(filename):
    landmarks = np.zeros((68, 2))
    with open(filename, 'r') as f:
        f.readline()
        f.readline()
        f.readline()
        for i in range(0, 68):
            line = f.readline()
            elems = line.split(' ')
            landmarks[i, :] = np.array([float(elems[0]), float(elems[1])])
    return landmarks

def saveIterationsLandmarks(filename, landmarksDict):
    np.save(open(filename, 'wb+'), landmarksDict)

    #serializableDict = {}
    #for (key, value) in landmarksDict.items():
    #    serializableList = [landmarks.flatten().tolist() for landmarks in value]
    #    serializableDict.update({key: serializableList})

    #json.dump(serializableDict, open(filename, 'w+'), sort_keys=True, indent=4)

def meanShape(filename):
    shape = np.zeros((68, 2))
    with open(filename, 'r') as f:
        for i in range(0, 68):
            line = f.readline()
            elems = line.split(' ')
            shape[i, :] = np.array([float(elems[1]), float(elems[2])])
    return shape

def rectAndCenterFromLandmarks(landmarks):
    l, t = np.amin(landmarks, axis=0)
    r, b = np.amax(landmarks, axis=0)
    rect = np.array([l, t, r - l, b - t])
    return rect, rectCenter(rect)


def transformLandmarks(landmarks, rect, center):
    landmarksRect, landmarksCenter = rectAndCenterFromLandmarks(landmarks)
    factor = rect[3] / landmarksRect[3]
    landmarks -= landmarksCenter
    landmarks *= factor
    landmarks += center
    return landmarks

def gtLandmarksFromDirectories(dirs) :
    for dir in dirs:
        directory = os.fsencode(dir)
        for file in os.listdir(directory):
            filename = os.fsdecode(file)
            if filename.endswith(".pts"):
                absFilename = os.path.abspath(dir) + '/' + filename
                absFilenameWithoutExt = os.path.splitext(absFilename)[0]
                imageAbsFilename = absFilenameWithoutExt + '.png'
                if not os.path.isfile(imageAbsFilename):
                    imageAbsFilename = absFilenameWithoutExt + '.jpg'
                yield imageAbsFilename, readGTLandmarks(absFilename)


def rectForTargetSize(rect, targetSize):
    rectMaxDimension = max([rect[2], rect[3]])
    factor = targetSize / rectMaxDimension
    scaledRect = np.around(rect * factor)
    return scaledRect, factor

def displayRectAndCenter(rect, img):
    copy = np.copy(img)
    p1 = tuple(np.around(rect[0:2]).astype(int))
    p2 = tuple(np.around(rect[0:2] + rect[2:4]).astype(int))
    cv2.rectangle(copy, p1, p2, (255, 0, 0, 255))
    center = tuple([int(sum(x) / 2) for x in zip(p1,p2)])
    cv2.circle(copy, center, 1, (255, 0, 0, 255))
    cv2.imshow('lala', copy)
    cv2.waitKey(0)

#todo clamp size to the original image size
def imageScaledAsBoundingRectOfTargetSize(img, landmarks, targetSize) :
    rect, center = rectAndCenterFromLandmarks(landmarks)
    #displayRectAndCenter(rect, img)
    scaledRect, factor = rectForTargetSize(rect, targetSize)
    rectWithPadding = np.around(rectCenteredAt(center, 1.5 * rect[2:4])).astype(int)
    rectWithPadding = clampRectBySize(rectWithPadding, np.array([img.shape[1], img.shape[0]]))
    #displayRectAndCenter(rectWithPadding, img)
    croppedImage = img[rectWithPadding[1]:rectWithPadding[1] + rectWithPadding[3],
                       rectWithPadding[0]:rectWithPadding[0] + rectWithPadding[2]]
    resizedImg = cv2.resize(croppedImage, (int(round(rectWithPadding[2] * factor)),
                                           int(round(rectWithPadding[3] * factor))))
    targetCenter = (center - rectWithPadding[0:2]) * factor
    return resizedImg, transformLandmarks(landmarks, rectCenteredAt(targetCenter, scaledRect[2:4]),
                                          targetCenter)

def showLandmarksOnimage(landmarks, image, color):
    copy = np.copy(image)
    for landmark in landmarks:
        cv2.circle(copy, (int(landmark[0]), int(landmark[1])), 3, color, -1)
    cv2.imshow('lala', copy)
    cv2.waitKey(0)

def rectCenteredAt(center, size):
    return np.append(center - size / 2, size, axis=0)

def rectCenter(rect):
    return rect[0:2] + rect[2:4] / 2

def clampRectBySize(rect, size):
    tl = np.array([clamp(rect[0], 0, size[0]), clamp(rect[1], 0, size[1])])
    br = rect[0:2] + rect[2:4]
    br = np.array([clamp(br[0], 0, size[0]), clamp(br[1], 0, size[1])])
    return np.concatenate((tl, br - tl))

def clamp(value, minimum, maximum):
    return min(max(value, minimum), maximum)

#ground truth points is array of size [2, 68]
#iterations landmarks is a matrix of size [10, 136]

def initDatabase(directories, meanshape, targetSize, targetDir):
    for absFilename, gtLandmarks in gtLandmarksFromDirectories(directories):
        image = cv2.imread(absFilename)
        scaledImage, scaledGTLandmarks = imageScaledAsBoundingRectOfTargetSize(image, gtLandmarks, targetSize)

        fileNameWithoutExt = os.path.splitext(os.path.basename(absFilename))[0]
        targetFileNameWithoutEx = targetDir + '/' + os.path.basename(fileNameWithoutExt)

        cv2.imwrite(targetFileNameWithoutEx + '.jpg', scaledImage)

        gtScaledRect, gtScaledCenter = rectAndCenterFromLandmarks(scaledGTLandmarks)

        shapes = np.zeros((10, scaledGTLandmarks.size), float)
        for i in range(0, 10):
            factor = np.random.normal([0, 0, 1], [0.02, 0.02, 0.02])
            targetCenter = gtScaledCenter + targetSize * factor[0:2]
            targetRect = rectCenteredAt(targetCenter, gtScaledRect[2:4] * factor[2])
            targetShape = np.around(transformLandmarks(meanshape, targetRect, targetCenter)).flatten()
            #showLandmarksOnimage(targetShape, scaledImage, (255, 0, 0, 255))
            shapes[i, :] = targetShape

        dictionary = {'gt': [np.around(scaledGTLandmarks)], '0': shapes}
        landmarksFileName = targetFileNameWithoutEx + '.its'
        saveIterationsLandmarks(landmarksFileName, dictionary)

def imageSamplesAndGTFromDirectory(dir, iterationKey):
    for absFilename in samplesInDir(dir):
        absFilenameWithoutExt = os.path.splitext(absFilename)[0]
        imageAbsFilename = absFilenameWithoutExt + '.jpg'
        dictionary = np.load(open(absFilename, 'rb'))[()]
        gt = dictionary['gt'][0]
        samples = dictionary[str(iterationKey)]
        yield cv2.imread(imageAbsFilename), samples, gt

def samplesInDir(dir):
    directory = os.fsencode(dir)
    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        if filename.endswith(".its"):
            yield os.path.abspath(dir) + '/' + filename
