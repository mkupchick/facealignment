import Landmarks
import cv2
import numpy as np
import Features

# add 1 to landmarks to train for bias term too.

#plan

#dimensions are always rows x columns

#read files divide train and test

# unknown regression matrix is nLandmarks columns on nFeatures rows since it tranforms the features to new landmark
# positions
# features * W = landmarks
# we need to solve F * W = L where F is samples X nFeatures, W is nFeatures x nLandmarks and L = samples x nLandmarks

# for each iteration
#   for each image
#       scale rectangle of ground truth
#       generate initial positions by shifting and scaling the initial mask by known distribution
#       calculate features for each position
#       append features row to F and ground truth landmarks to matrix L
#   compute pseudo inverse of F and multiply it by L -> this yields W.
#   repeat for next iteration

#todo: refactor to work with batch getting input matrix and adding rows
#todo: use list of input image pairs and landmarks from previous run
#todo: rescale mean landmarks outside this fundction
#todo: try to use faster features
def regressionMatrix(dir, iteration, samplesNum, featuresSize, landmarksSize):
    samplesFeatures = np.zeros((samplesNum, featuresSize + 1), float)
    samplesFeatures[:, featuresSize:featuresSize+1] = 1
    gtList = np.zeros((samplesNum, landmarksSize), float)

    sampleIndex = 0
    for image, samples, gtLandmarks in Landmarks.imageSamplesAndGTFromDirectory(dir, iteration):
        #Landmarks.showLandmarksOnimage(gtLandmarks, image, (255, 0, 0, 255))
        for landmarks in samples:
            #Landmarks.showLandmarksOnimage(landmarks, image, (0, 0, 255, 255))

            landmarksPoints = np.resize(landmarks, (int(landmarks.size / 2), 2))
            siftFeatures = Features.siftFromLandmarks(image, landmarksPoints).flatten()
            samplesFeatures[sampleIndex, 0:featuresSize] = siftFeatures

            gtRow = gtLandmarks.flatten() - landmarks
            gtList[sampleIndex, :] = gtRow
            sampleIndex += 1

    F = samplesFeatures
    L = gtList
    covarianceMatF = np.dot(F.transpose(), F)
    pseudoInverse = np.dot(np.linalg.inv(covarianceMatF), F.transpose())
    result = np.matmul(pseudoInverse, L)
    return result, F
