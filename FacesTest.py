import numpy as np
import Landmarks
import Features


dir = 'train'

a = list(range(0, 5))
print(a[0:1])

matrices = []

for i in range(0, 5):
    matrix = np.load('matrix' + str(i+1) + '.mat')
    matrices.append(matrix)

featuresVector = np.zeros(matrices[0].shape[0], float)
featuresVector[featuresVector.size-1] = 1

landmarks = np.zeros(matrices[0].shape[1], float)

for image, samples, gtLandmarks in Landmarks.imageSamplesAndGTFromDirectory(dir, 0):
    landmarks[:] = samples[0]
    landmarksPoints = np.resize(landmarks, (int(landmarks.size / 2), 2))
    Landmarks.showLandmarksOnimage(landmarksPoints, image, (255, 0, 0, 255))
    for matrix in matrices:
        featuresVector[0:featuresVector.size-1] = Features.siftFromLandmarks(image, landmarksPoints).flatten()
        delta = np.dot(featuresVector, matrix)
        landmarks[:] = landmarks + delta
        landmarksPoints = np.resize(landmarks, (int(landmarks.size / 2), 2))
        Landmarks.showLandmarksOnimage(landmarksPoints, image, (255, 0, 0, 255))

landmarksPoints = np.resize(landmarks, (int(landmarks.size / 2), 2))
Landmarks.showLandmarksOnimage(landmarksPoints, image, (255, 0, 0, 255))





