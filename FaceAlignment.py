import numpy as np
import cv2
import Landmarks
import SDM

directories = ['300W/01_Indoor', '300W/02_Outdoor', 'afw', 'lfpw/trainset']

def transformLandmarks(regressionMatrix, featuresMatrix, curLandmarks):
    deltas = np.dot(featuresMatrix, regressionMatrix)
    return np.around(deltas + curLandmarks)

targetSize = 256

# training
# generate init landmarks for each image
#meanLandmarks = Landmarks.meanShape('meanshape.txt')
#Landmarks.initDatabase(directories, meanLandmarks, targetSize, 'train')

#exit()


#rm = np.zeros((100, 20), float)
#fm = np.zeros((10, 100), float)
#cl = np.zeros((10, 20), float)

#transformLandmarks(rm, fm, cl)

trainDir = 'train'
matrices = {}
#dict = {'0': np.array([1, 2, 3])}
#np.save(open('temp.txt', 'wb+'), dict)
#dict1 = np.load(open('temp.txt', 'rb'))

istart = 0

for i in range(istart, 5):
    matrix, featuresMatrix = SDM.regressionMatrix(trainDir, i, 17480, 8704, 136)
    np.save(open('matrix' + str(i+1) + '.mat', 'wb+'), matrix)

    print('saved ' + str(i) + ' iteration')
    featuresIndex = 0

    for samplesFile in Landmarks.samplesInDir(trainDir):
        samplesDictionary = np.load(open(samplesFile, 'rb'))[()]

        featuresStart = featuresIndex * 10
        featuresMatrixForImage = featuresMatrix[featuresStart:featuresStart + 10, :]

        nextLandmarks = transformLandmarks(matrix, featuresMatrixForImage, samplesDictionary[str(i)])
        samplesDictionary.update({str(i + 1): nextLandmarks})
        np.save(open(samplesFile, 'wb+'), samplesDictionary)
        featuresIndex += 1
    print('saved landmarks')

print('finished')
