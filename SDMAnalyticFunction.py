import numpy as np

x = np.arange(-1, 1, 0.2)
features = np.sin(x)

rs = np.zeros(5, float)

for i in range(0, 5):
    r = np.dot(np.linalg.inv(np.dot(features.transpose(), features)), features.transpose())
    rs[i] = r


test = np.arange(-1, 1, 0.05)

curValues = np.array(test)

for i in range(0, 5):
    curValue = curValues * rs[i]